mod common;

use rocket_tp::{repository::show_repository::ShowRepository, core::repository::Repository, entity::show::Show};
use serial_test::serial;

use crate::common::init_repositories;

static SHOW_COUNT: usize = 8807;
static FIND_BY_ID_TEST_ID: usize = 10;

#[test]
#[serial]
pub fn find_all_has_all_shows(){
    init_repositories();
    
    assert_eq!(ShowRepository::find_all().unwrap().len(), SHOW_COUNT);
}

#[test]
#[serial]
pub fn find_by_id_retrieves_right_id(){
    init_repositories();

    assert_eq!(ShowRepository::find_by_id(FIND_BY_ID_TEST_ID).unwrap().unwrap().id, FIND_BY_ID_TEST_ID);
}

#[test]
#[serial]
pub fn insert_adds_one_to_show_count(){
    init_repositories();

    ShowRepository::insert(Show::default()).unwrap();

    assert_eq!(ShowRepository::find_all().unwrap().len(), SHOW_COUNT + 1);
}

#[test]
#[serial]
pub fn delete_subs_one_from_show_count(){
    init_repositories();

    assert!(ShowRepository::delete(FIND_BY_ID_TEST_ID).unwrap());

    assert_eq!(ShowRepository::find_all().unwrap().len(), SHOW_COUNT - 1);
}

#[test]
#[serial]
pub fn update_changes_entity(){
    init_repositories();

    let previous_show = ShowRepository::find_by_id(FIND_BY_ID_TEST_ID).unwrap().unwrap().value;
    ShowRepository::update(FIND_BY_ID_TEST_ID, Show::default()).unwrap();

    assert_ne!(ShowRepository::find_by_id(FIND_BY_ID_TEST_ID).unwrap().unwrap().value, previous_show);
}
