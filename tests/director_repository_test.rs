mod common;

use rocket_tp::{repository::director_repository::DirectorRepository, core::repository::Repository, entity::director::Director};
use serial_test::serial;

use crate::common::init_repositories;

static DIRECTOR_COUNT: usize = 4994;
static FIND_BY_ID_TEST_ID: usize = 10;

#[test]
#[serial]
pub fn find_all_has_all_shows(){
    init_repositories();
    
    assert_eq!(DirectorRepository::find_all().unwrap().len(), DIRECTOR_COUNT);
}

#[test]
#[serial]
pub fn find_by_id_retrieves_right_id(){
    init_repositories();

    assert_eq!(DirectorRepository::find_by_id(FIND_BY_ID_TEST_ID).unwrap().unwrap().id, FIND_BY_ID_TEST_ID);
}

#[test]
#[serial]
pub fn insert_adds_one_to_show_count(){
    init_repositories();

    DirectorRepository::insert(Director::default()).unwrap();

    assert_eq!(DirectorRepository::find_all().unwrap().len(), DIRECTOR_COUNT + 1);
}

#[test]
#[serial]
pub fn delete_subs_one_from_show_count(){
    init_repositories();

    assert!(DirectorRepository::delete(FIND_BY_ID_TEST_ID).unwrap());

    assert_eq!(DirectorRepository::find_all().unwrap().len(), DIRECTOR_COUNT - 1);
}

#[test]
#[serial]
pub fn update_changes_entity(){
    init_repositories();

    let previous_show = DirectorRepository::find_by_id(FIND_BY_ID_TEST_ID).unwrap().unwrap().value;
    DirectorRepository::update(FIND_BY_ID_TEST_ID, Director::default()).unwrap();

    assert_ne!(DirectorRepository::find_by_id(FIND_BY_ID_TEST_ID).unwrap().unwrap().value, previous_show);
}
