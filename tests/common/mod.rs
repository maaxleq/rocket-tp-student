use std::fs;

use rocket_tp::app;

static JSON_PATH: &str = "show_data/netflix.json";

pub fn init_repositories(){
    let json_string = fs::read_to_string(JSON_PATH).unwrap();
    app::init(json_string);
}
