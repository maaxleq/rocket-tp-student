#![feature(decl_macro)]

#[macro_use]
extern crate rocket;

pub mod app;
pub mod controller;
pub mod core;
pub mod entity;
pub mod error;
pub mod repository;
pub mod service;
