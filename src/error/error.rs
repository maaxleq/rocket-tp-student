use rocket::{http::Status, response::Responder, Response};

#[derive(Debug)]
pub struct AppError {
    pub message: String,
    pub status: Status,
}

impl<'r> Responder<'r> for AppError {
    fn respond_to(self, request: &rocket::Request) -> rocket::response::Result<'r> {
        Response::build_from(self.message.respond_to(request)?)
            .status(self.status)
            .ok()
    }
}
