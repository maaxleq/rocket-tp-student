use serde_json::Value;

use crate::{
    core::repository::{Record, Repository},
    entity::director::Director,
    error::error::AppError,
    repository::director_repository::DirectorRepository,
};

pub struct DirectorService {}

impl DirectorService {
    pub fn find_by_id(id: usize) -> Result<Option<Record<Director, usize>>, AppError> {
        DirectorRepository::find_by_id(id)
    }

    pub fn find_all() -> Result<Vec<Record<Director, usize>>, AppError> {
        DirectorRepository::find_all()
    }

    pub fn find_one_by_name(name: String) -> Result<Option<Record<Director, usize>>, AppError> {
        DirectorRepository::find_one_by("name", Value::String(name))
    }

    pub fn find_many_by_name_containing(
        name: String,
    ) -> Result<Vec<Record<Director, usize>>, AppError> {
        DirectorRepository::find_many_by("name_containing", Value::String(name))
    }

    pub fn insert(director: Director) -> Result<Option<Record<Director, usize>>, AppError> {
        DirectorRepository::insert(director)
    }

    pub fn update(
        id: usize,
        director: Director,
    ) -> Result<Option<Record<Director, usize>>, AppError> {
        DirectorRepository::update(id, director)
    }

    pub fn delete(id: usize) -> Result<bool, AppError> {
        DirectorRepository::delete(id)
    }
}
