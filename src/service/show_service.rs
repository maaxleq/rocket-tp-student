use serde_json::Value;

use crate::{
    core::repository::{Record, Repository},
    entity::show::Show,
    error::error::AppError,
    repository::show_repository::ShowRepository,
};

pub struct ShowService {}

impl ShowService {
    pub fn find_by_id(id: usize) -> Result<Option<Record<Show, usize>>, AppError> {
        ShowRepository::find_by_id(id)
    }

    pub fn find_all() -> Result<Vec<Record<Show, usize>>, AppError> {
        ShowRepository::find_all()
    }

    pub fn find_one_by_title(title: String) -> Result<Option<Record<Show, usize>>, AppError> {
        ShowRepository::find_one_by("title", Value::String(title))
    }

    pub fn find_many_by_title_containing(
        title: String,
    ) -> Result<Vec<Record<Show, usize>>, AppError> {
        ShowRepository::find_many_by("title_containing", Value::String(title))
    }

    pub fn insert(show: Show) -> Result<Option<Record<Show, usize>>, AppError> {
        ShowRepository::insert(show)
    }

    pub fn update(id: usize, show: Show) -> Result<Option<Record<Show, usize>>, AppError> {
        ShowRepository::update(id, show)
    }

    pub fn delete(id: usize) -> Result<bool, AppError> {
        ShowRepository::delete(id)
    }
}
