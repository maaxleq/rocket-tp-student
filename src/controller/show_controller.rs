use rocket::http::Status;
use rocket_contrib::json::Json;

use crate::{
    core::repository::Record, entity::show::Show, error::error::AppError,
    service::show_service::ShowService,
};

#[get("/")]
pub fn find_all() -> Result<Json<Vec<Record<Show, usize>>>, AppError> {
    Ok(Json(ShowService::find_all()?))
}

#[get("/title?<title>")]
pub fn find_one_by_title(title: String) -> Result<Json<Record<Show, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 13: Appeler le service des films et séries pour la recherche par titre exact et renvoyer la réponse au client
    */
}

#[get("/title/search?<title>")]
pub fn find_many_by_title_containing(
    title: String,
) -> Result<Json<Vec<Record<Show, usize>>>, AppError> {
    todo!()
    /*
        EXERCICE 14: Appeler le service des films et séries pour la recherche par titre contenu et renvoyer la réponse au client
    */
}

#[get("/<id>")]
pub fn find_by_id(id: usize) -> Result<Json<Record<Show, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 15: Appeler le service des films et séries pour la recherche par id et renvoyer la réponse au client
    */
}

#[post("/", data = "<show_json>")]
pub fn insert(show_json: Json<Show>) -> Result<Json<Record<Show, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 16: Appeler le service des films et séries pour l'insertion et renvoyer la réponse au client
    */
}

#[put("/<id>", data = "<show_json>")]
pub fn update(id: usize, show_json: Json<Show>) -> Result<Json<Record<Show, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 17: Appeler le service des films et séries pour la mise à jour et renvoyer la réponse au client
    */
}

#[delete("/<id>")]
pub fn delete(id: usize) -> Result<(), AppError> {
    todo!()
    /*
        EXERCICE 18: Appeler le service des films et séries pour la suppression et renvoyer la réponse au client
    */
}
