use rocket::http::Status;
use rocket_contrib::json::Json;

use crate::{
    core::repository::Record, entity::director::Director, error::error::AppError,
    service::director_service::DirectorService,
};

#[get("/")]
pub fn find_all() -> Result<Json<Vec<Record<Director, usize>>>, AppError> {
    Ok(Json(DirectorService::find_all()?))
}

#[get("/name?<name>")]
pub fn find_one_by_name(name: String) -> Result<Json<Record<Director, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 7: Appeler le service des réalisateurs pour la recherche par nom exact et renvoyer la réponse au client
    */
}

#[get("/name/search?<name>")]
pub fn find_many_by_name_containing(
    name: String,
) -> Result<Json<Vec<Record<Director, usize>>>, AppError> {
    todo!()
    /*
        EXERCICE 8: Appeler le service des réalisateurs pour la recherche par nom contenu et renvoyer la réponse au client
    */
}

#[get("/<id>")]
pub fn find_by_id(id: usize) -> Result<Json<Record<Director, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 9: Appeler le service des réalisateurs pour la recherche par id et renvoyer la réponse au client
    */
}

#[post("/", data = "<director_json>")]
pub fn insert(director_json: Json<Director>) -> Result<Json<Record<Director, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 10: Appeler le service des réalisateurs pour l'insertion et renvoyer la réponse au client
    */
}

#[put("/<id>", data = "<director_json>")]
pub fn update(
    id: usize,
    director_json: Json<Director>,
) -> Result<Json<Record<Director, usize>>, AppError> {
    todo!()
    /*
        EXERCICE 11: Appeler le service des réalisateurs pour la mise à jour et renvoyer la réponse au client
    */
}

#[delete("/<id>")]
pub fn delete(id: usize) -> Result<(), AppError> {
    todo!()
    /*
        EXERCICE 12: Appeler le service des réalisateurs pour la suppression et renvoyer la réponse au client
    */
}
