static ERROR_JSON_CONVERT_ERROR: &str = "Could not convert JSON file";

use std::collections::HashSet;

use crate::controller::director_controller;
use crate::core::repository::Repository;
use crate::entity::director::Director;
use crate::repository::director_repository::DirectorRepository;
use crate::{
    controller::show_controller, entity::show::Show, repository::show_repository::ShowRepository,
};

pub fn start(json_data: String) {
    init(json_data);

    rocket::ignite()
        .mount(
            "/show",
            routes![
                show_controller::find_all,
                show_controller::find_by_id,
                show_controller::find_many_by_title_containing,
                show_controller::find_one_by_title,
                show_controller::insert,
                show_controller::update,
                show_controller::delete
            ],
        )
        .mount(
            "/director",
            routes![
                director_controller::find_all,
                director_controller::find_by_id,
                director_controller::find_many_by_name_containing,
                director_controller::find_one_by_name,
                director_controller::insert,
                director_controller::update,
                director_controller::delete
            ],
        )
        .launch();
}

pub fn init(json_data: String) {
    let shows: Vec<Show> =
        serde_json::from_str(json_data.as_str()).expect(&format!("{}", ERROR_JSON_CONVERT_ERROR));
    ShowRepository::init(shows);

    let directors = todo!();
    /*
        EXERCICE 1: Implémenter l'initialisation du repository des réalisateurs
        Créer un HashSet et insérer les noms des réalisateurs(trices) sans dupliqués
        Pour récupérer les noms des réalisateurs(trices), séparer le champs 'directors' de chaque 'Show',
        avec pour séparateur la chaîne " ,"
    */

    DirectorRepository::init(directors);
}
