use std::{env, fs};

use rocket_tp::app;

static ERROR_ARGUMENT_PARSE: &str = "Could not parse argument";
static ERROR_NO_SOURCE_PROVIDED: &str = "No data source provided";
static ERROR_FILE_READ: &str = "Could not read file";

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut json_opt: Option<String> = None;

    for arg in args.iter().skip(1) {
        if arg.starts_with("json=") {
            json_opt = Some(arg[5..arg.len()].to_string());
        } else {
            panic!("{} {}", ERROR_ARGUMENT_PARSE, arg);
        }
    }

    let json_data: String = match json_opt {
        Some(json) => fs::read_to_string(&json).expect(&format!("{} {}", ERROR_FILE_READ, &json)),
        None => {
            panic!("{}", ERROR_NO_SOURCE_PROVIDED);
        }
    };

    app::start(json_data);
}
