use serde::{Deserialize, Serialize};

use crate::core::entity::Entity;

#[derive(Serialize, Deserialize, Clone, Debug, Default, Eq, PartialEq)]
pub struct Show {
    #[serde(alias = "type")]
    pub show_type: String,
    pub title: String,
    #[serde(alias = "director")]
    pub directors: String,
    pub cast: String,
    pub country: String,
    pub release_year: u32,
    pub rating: String,
    pub duration: String,
    #[serde(alias = "listed_in")]
    pub categories: String,
    pub description: String,
}

impl Entity for Show {}
