use serde::{Deserialize, Serialize};

use crate::core::entity::Entity;

#[derive(Serialize, Deserialize, Clone, Debug, Default, Eq, PartialEq)]
pub struct Director {
    pub name: String,
}

impl Entity for Director {}
