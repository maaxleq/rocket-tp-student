use std::{collections::HashMap, sync::Mutex};

use rocket::http::Status;
use serde_json::Value;

use crate::{
    core::repository::{Record, Repository},
    entity::director::Director,
    error::error::AppError,
};

static ERROR_DEREF_DATA_VECTOR: &str = "Could not dereference the data vector";
static ERROR_DEREF_NEXT_ID: &str = "Could not dereference the next id";
static ERROR_UNSUPPORTED_CONDITION: &str = "Attempted to find by an unsupported condition";
static ERROR_NOT_STRING: &str = "Value is not a string";

static DIRECTORS_STORAGE: Mutex<Option<HashMap<usize, Director>>> = Mutex::new(None);
static LAST_ID: Mutex<usize> = Mutex::new(0);

pub struct DirectorRepository {}

impl Repository<Director, usize> for DirectorRepository {
    fn set_data(data: HashMap<usize, Director>) {
        let mut lock_result = DIRECTORS_STORAGE.lock();
        let directors_deref: &mut Option<HashMap<usize, Director>> =
            lock_result.as_deref_mut().expect(ERROR_DEREF_DATA_VECTOR);
        *directors_deref = Some(data);
    }

    fn access() -> Result<HashMap<usize, Director>, AppError> {
        match DIRECTORS_STORAGE
            .lock()
            .as_deref()
            .expect(ERROR_DEREF_DATA_VECTOR)
        {
            Some(vec) => Ok(vec.clone()),
            None => Err(AppError {
                message: String::from("Director repository is not initialized"),
                status: Status::InternalServerError,
            }),
        }
    }

    fn find_one_by(
        condition: &str,
        value: Value,
    ) -> Result<std::option::Option<Record<Director, usize>>, AppError> {
        let directors = Self::access()?;

        todo!()

        /*
            EXERCICE 5: Implémenter une recherche par nom exact (condition = "name") qui retourne un ou zéro résultat
        */
    }

    fn find_many_by(
        condition: &str,
        value: Value,
    ) -> Result<Vec<Record<Director, usize>>, AppError> {
        let directors = Self::access()?;

        todo!()
        /*
            EXERCICE 6: Implémenter une recherche par nom contenu (condition = "name_containing") qui retourne plusieurs résultats
            Par exemple, si la value est "Steven", un résultat pourra être "Steven Spielberg"
        */
    }

    fn next_id() -> usize {
        let mut id = LAST_ID.lock().expect(ERROR_DEREF_NEXT_ID);
        *id += 1;
        *id
    }

    fn reset_id() {
        let mut id = LAST_ID.lock().expect(ERROR_DEREF_NEXT_ID);
        *id = 0;
    }
}
