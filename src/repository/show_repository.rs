use std::{collections::HashMap, sync::Mutex};

use rocket::http::Status;
use serde_json::Value;

use crate::{
    core::repository::{Record, Repository},
    entity::show::Show,
    error::error::AppError,
};

static ERROR_DEREF_DATA_VECTOR: &str = "Could not dereference the data vector";
static ERROR_DEREF_NEXT_ID: &str = "Could not dereference the next id";
static ERROR_UNSUPPORTED_CONDITION: &str = "Attempted to find by an unsupported condition";
static ERROR_NOT_STRING: &str = "Value is not a string";

static SHOWS_STORAGE: Mutex<Option<HashMap<usize, Show>>> = Mutex::new(None);
static LAST_ID: Mutex<usize> = Mutex::new(0);

pub struct ShowRepository {}

impl Repository<Show, usize> for ShowRepository {
    fn set_data(data: HashMap<usize, Show>) {
        let mut lock_result = SHOWS_STORAGE.lock();
        let shows_deref: &mut Option<HashMap<usize, Show>> =
            lock_result.as_deref_mut().expect(ERROR_DEREF_DATA_VECTOR);
        *shows_deref = Some(data);
    }

    fn access() -> Result<HashMap<usize, Show>, AppError> {
        match SHOWS_STORAGE
            .lock()
            .as_deref()
            .expect(ERROR_DEREF_DATA_VECTOR)
        {
            Some(vec) => Ok(vec.clone()),
            None => Err(AppError {
                message: String::from("Show repository is not initialized"),
                status: Status::InternalServerError,
            }),
        }
    }

    fn find_one_by(
        condition: &str,
        value: Value,
    ) -> Result<std::option::Option<Record<Show, usize>>, AppError> {
        let shows = Self::access()?;

        match condition {
            "title" => {
                let id_entity_opt = shows.into_iter().find(|x| x.1.title == value);
                Self::get_record_opt_ok(id_entity_opt)
            }
            _ => Err(AppError {
                message: String::from(ERROR_UNSUPPORTED_CONDITION),
                status: Status::InternalServerError,
            }),
        }
    }

    fn find_many_by(condition: &str, value: Value) -> Result<Vec<Record<Show, usize>>, AppError> {
        let shows = Self::access()?;

        match condition {
            "title_containing" => match value.as_str() {
                Some(string) => {
                    let id_entity_map: HashMap<usize, Show> = shows
                        .into_iter()
                        .filter(|x| {
                            x.1.title
                                .to_lowercase()
                                .contains(string.to_lowercase().as_str())
                        })
                        .collect();

                    Self::get_records_hashmap_ok(id_entity_map)
                }
                None => Err(AppError {
                    message: String::from(ERROR_NOT_STRING),
                    status: Status::InternalServerError,
                }),
            },
            _ => Err(AppError {
                message: String::from(ERROR_UNSUPPORTED_CONDITION),
                status: Status::InternalServerError,
            }),
        }
    }

    fn next_id() -> usize {
        let mut id = LAST_ID.lock().expect(ERROR_DEREF_NEXT_ID);
        *id += 1;
        *id
    }

    fn reset_id() {
        let mut id = LAST_ID.lock().expect(ERROR_DEREF_NEXT_ID);
        *id = 0;
    }
}
