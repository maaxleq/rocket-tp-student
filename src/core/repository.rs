use std::{collections::HashMap, hash::Hash};

use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::error::error::AppError;

use super::entity::Entity;

#[derive(Serialize, Deserialize, Clone, Debug, Eq, PartialEq)]
pub struct Record<T: Entity, I: Clone + Eq + Hash> {
    pub id: I,
    pub value: T,
}

pub trait Repository<T: Entity + Clone, I: Clone + Eq + Hash> {
    fn init(source: Vec<T>) {
        Self::reset_id();
        let mut map = HashMap::new();

        for entity in source {
            map.insert(Self::next_id(), entity);
        }

        Self::set_data(map);
    }

    fn find_by_id(id: I) -> Result<Option<Record<T, I>>, AppError> {
        let entities = Self::access()?;

        let entity_opt = entities.get(&id);

        Self::get_record_opt_ok_sep_args(id, entity_opt)
    }

    fn find_all() -> Result<Vec<Record<T, I>>, AppError> {
        let entities = Self::access()?;

        Self::get_records_hashmap_ok(entities)
    }

    fn insert(entity: T) -> Result<Option<Record<T, I>>, AppError> {
        let mut entities = Self::access()?;

        let id = Self::next_id();

        if entities.contains_key(&id) {
            Ok(None)
        } else {
            todo!();
            /*
                EXERCICE 2: Insérer `entity` dans `entities`
            */

            Self::set_data(entities);

            Ok(Some(Record {
                id: id,
                value: entity,
            }))
        }
    }

    fn update(id: I, entity: T) -> Result<Option<Record<T, I>>, AppError> {
        let entities = Self::access()?;

        match Self::find_by_id(id.clone())? {
            Some(_) => {
                let entities_mapped: HashMap<I, T> = todo!();
                /*
                    EXERCICE 3: Mapper `entities` pour remplacer l'entité ayant l'id `id` par `entity`
                */

                Self::set_data(entities_mapped);

                Ok(Some(Record {
                    id: id,
                    value: entity,
                }))
            }
            None => Ok(None),
        }
    }

    fn delete(id: I) -> Result<bool, AppError> {
        let entities = Self::access()?;
        let start_length = entities.len();

        
        let entities_filtered: HashMap<I, T> = todo!();
        /*
            EXERCICE 4: Filtrer les entités pour supprimer l'entité ayant l'id passé en paramètre `id`
        */

        if entities_filtered.len() == start_length {
            Ok(false)
        } else {
            Self::set_data(entities_filtered);
            Ok(true)
        }
    }

    fn get_record_opt_ok_sep_args(
        id: I,
        entity_opt: Option<&T>,
    ) -> Result<Option<Record<T, I>>, AppError> {
        match entity_opt {
            Some(entity) => Ok(Some(Record {
                id: id,
                value: entity.clone(),
            })),
            None => Ok(None),
        }
    }

    fn get_record_opt_ok(id_entity_opt: Option<(I, T)>) -> Result<Option<Record<T, I>>, AppError> {
        match id_entity_opt {
            Some(id_entity) => Ok(Some(Record {
                id: id_entity.0,
                value: id_entity.1.clone(),
            })),
            None => Ok(None),
        }
    }

    fn get_records_hashmap_ok(entities: HashMap<I, T>) -> Result<Vec<Record<T, I>>, AppError> {
        let mut vec = Vec::new();

        for id_entity in entities {
            vec.push(Record {
                id: id_entity.0,
                value: id_entity.1,
            });
        }

        Ok(vec)
    }

    fn next_id() -> I;
    fn reset_id();
    fn set_data(data: HashMap<I, T>);
    fn access() -> Result<HashMap<I, T>, AppError>;
    fn find_one_by(condition: &str, value: Value) -> Result<Option<Record<T, I>>, AppError>;
    fn find_many_by(condition: &str, value: Value) -> Result<Vec<Record<T, I>>, AppError>;
}
