# TP Rust & Rocket

## Contexte

Le but de ce TP est de compléter une API exposant des films et séries Netflix, ainsi que leurs réalisateurs et réalisatrices.

Les données sont chargées depuis un fichier JSON contenant des données sur des films et séries Netflix. Les réalisateurs(trices) sont extraits depuis chaque film ou série.

## Architecture

L'architecture se base sur un modèles *contrôleur -> service -> repository*, où les services ne connaissent pas les contrôleurs et les repositories ne connaissent ni les services, ni les contrôleurs.

Il y a un ensemble *contrôleur -> service -> repository* pour chaque entité (**Show** et **Director**).

Les contrôleurs reçoivent les requêtes HTTP, appellent les services, qui appellent les repositories. Une fois l'information remontée, les contrôleurs renvoient une réponse au client.

Les contrôleurs sont montés sur un objet Rocket, selon des routes de base (**/show** pour les films et séries, et **/director** pour les réalisateurs).

## Lexique

- Director = Réalisateur(trice)
- Show = Film ou série

## Tests

Des tests sont à disposition pour tester les repositories. Lancez les tests avec la commande **cargo test --test "*"** à la racine du projet.

## Exercices

Pour chaque exercice (noté **EXERCICE [N]** en commentaire), remplacer le **todo!()** par du code.

***Tip**: Dans votre IDE, recherchez le texte **EXERCICE [N]** pour aller à l'exercice.*

### Exercice 1

Implémenter l'initialisation du repository des réalisateurs.
Créer un **HashSet** et insérer les noms des réalisateurs(trices) sans dupliqués.
Pour récupérer les noms des réalisateurs(trices), splitter le champs **directors** de chaque **Show**, avec pour séparateur la chaîne **" ,"**.

### Exercice 2

Implémenter l'insertion de la variable **entity** dans **entities**, pour sauvegarder l'entité.

### Exercice 3

Mapper **entities** pour mettre à jour l'entité ayant la clé **id** avec la variable **entity**.

***Tip**: Utiliser les méthodes **into_iter()**, **map()** et **collect().***

### Exercice 4

Filtrer **entities** pour supprimer l'entrée ayant la clé **id**.

### Exercice 5

Implémenter une recherche par nom exact (**condition** = **"name"**) qui retourne un ou zéro résultat.

***Tip**: Un **find()** vous permettra d'avoir une vraiable de type **Option<(usize, Director)>**. Passez cette variable dans la fonction **Self::get_record_opt_ok(id_entity_opt)** pour la transformer en **Result<std::option::Option<Record<Director, usize>>, AppError>**, afin de correspondre à la signature de la méthode du repository.*

### Exercice 6

Implémenter une recherche par nom contenu (**condition** = **"name_containing"**) qui retourne plusieurs résultats.
Par exemple, si la **value** est **"Steven"**, un des résultats pourra être "Steven Spielberg".

***Tip**: Inspirez vous de l'implémentation de **show_repository**.*

### Exercice 7 à 12

Implémenter les contrôleurs pour les réalisateurs(trices).

***Tip**: Appelez le service **director_service** et faites un pattern matching sur le résultat de l'appel au service.*

### Exercice 13 à 18

Implémenter les contrôleurs pour les films et séries.

***Tip**: Appelez le service **show_service** et faites un pattern matching sur le résultat de l'appel au service.*
